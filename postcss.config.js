module.exports =  {
  plugins: [
    require('postcss-smart-import')(),
    require('postcss-cssnext'),
    require('postcss-responsive-type')()
  ]
};
