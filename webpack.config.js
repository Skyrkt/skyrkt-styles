const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const webpackConfig = {

  entry: {
    main: path.join(__dirname,'src/app.ts')
  },

  output: {
    path: path.join(__dirname, 'public'),
    filename: '[name].js'
  },

  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx']
  },

  devtool: 'sourcemap',

  module: {
    loaders: [
      {
        test: /\.ts$/,
        loader: 'awesome-typescript-loader'
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({
          fallbackLoader: 'style-loader',
          loader: [
            'css-loader?sourcemap&importLoaders=1',
            'postcss-loader'
          ]
        })
      },
      {
        test: /\.(jpg|png|gif)$/,
        loader: 'file-loader'
      }
    ]
  },

  plugins: [
    new ExtractTextPlugin('app.css'),
    new StyleLintPlugin({
      files: ['src/**/*.css'],
      failOnError: false,
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.join(__dirname,'src/index.html')
    })
  ]
};

if (process.env.ENV) {
  webpackConfig.plugins.push([
    new webpack.optimize.UglifyJsPlugin(),
    new OptimizeCssAssetsPlugin()
  ]);
}

module.exports = webpackConfig;
